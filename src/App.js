import { useState } from 'react';

import './App.css';
import { sections, group } from './data.js'

import { Button } from '@material-ui/core';
import { Paper } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    paper: {
      backgroundColor: 'white',
      padding: theme.spacing(5),
      width: '30vw',
      textAlign: 'center',
      color: '#444444'
    },
    turn: {
      fontSize: '20px'
    }
  }));

function App() {
  const classes = useStyles();
  const [ state, setState ] = useState({});
  const [ notEmpty, setNotEmpty ] = useState(true);
  const [ counter, setCounter ] = useState(0);
  const [ turn, setTurn ] = useState('');

  const getQuestionOnClick = (data) => {
    const sectionsNumber = Object.keys(data).length;
    const randomSectionIndex = Math.floor(Math.random() * sectionsNumber);
    const section = Object.keys(data)[randomSectionIndex];
  
    const questionsNumber = data[section].length;
    const randomQuestionsIndex = Math.floor(Math.random() * questionsNumber);
    const question = data[section][randomQuestionsIndex];

    const isNotEmpty = Object.values(data).some(el => el[0]);
    setNotEmpty(isNotEmpty);

    while(!question && isNotEmpty) {
      return getQuestionOnClick(data);
    }

    return {
      section,
      question,
    }
  }

  const currentTurn = (people) => {
    setTurn(people[counter]);

    (counter === people.length - 1) ? 
      setCounter(0) :
      setCounter(prev => prev + 1);
  }

  
  const handleGetQuestionClick = () => {
    const filtered = state.section && sections[state.section]
      .filter(el => el !== state.question);
    state.section && (sections[state.section] = filtered);

    const current = getQuestionOnClick(sections);
    setState(current);

    currentTurn(group);
  }

  return (
    <div className='App'>
        {!notEmpty ? (
          <Paper elevation={3} className={classes.paper}>You answered all questions!</Paper>
        ) : (
          <Paper elevation={3} className={classes.paper}>
            {state.question && (<>
            <p className={classes.turn}>{turn}'s turn</p>
            <h3>{state.question}</h3>
            <p>Question from {state.section}</p>
            </>)}
            <Button onClick={handleGetQuestionClick}>Ask a question</Button>
          </Paper>
        )}
    </div>
  );
}

export default App;
