export const sections = {
    'Loops/Arrays': [
        'What is a loop? What types of loops do you know?',
        'What is an array? What are the benefits of using arrays over using several variables?'
    ],
    'Functions/Exceptions': [
        'What is a function? What types of functions are there?',
        'What is the difference between function expression, fat arrows and function declaration?',
        'What is an immediately-invoked function expression? Why use it?',

    ],
    'Git': [
        `What is git? What are the benefits of using it?`,
        `What is GitLab? What are the benefits of using it?`,
        `How is GitLab different from git?`,
        `Which are the global configurations you need to set in order to use git? What is their role?`,
        `What is a commit? What information does it contain?`,
    ],
    'NPM/ES Modules': [
        `What is NPM? What are the benefits of using it?`,
        `What is the package.json file?`,
        `What are NPM scripts?`,
        `What are ES Modules?`,
    ],
    'Objects': [
        `What are objects? Provide an example.`,
        `What are computed properties?`,
        `What are property descriptors?`,
    ],
    'ESNext/ESLint': [
        `What are the benefits of including 'use strict' at the beginning of a JavaScript source file?`,
        `What are template strings? What are their benefits?`,
        `What are destructuring assignments?`,
    ],
    'Scope/Closure': [
        `What is scope? Provide an example.`,
        `What is the difference between const, let and var?`,
        `What is the difference between == and ===, and what is type coercion?`,
        `What is variable and function hoisting?`,
        `What is hoisted and what is not?`,
        `What is lexical scope? What is lexing-time?`,
        `What is closure? Provide an example.`,
    ],
    'Module Pattern/FP Intro': [
        `What are the benefits of using array methods (forEach, map, etc.) over loops?`,
        `What is a pure function and what are side effects?`,
        `What is a module and why would you use it?`,
        `What is the revealing module pattern?`,        
    ],
    'HTML': [
        `What is HTML?`,
        `What is a browser? How is it different from node.js?`,
        `What is semantic HTML? How do we achieve it?`,
    ],
    'CSS': [
        `What is CSS? What problems does it solve?`,
        `Which are the three ways of inserting CSS into a web page?`,
        `What is the difference between ids and classes? When to use what?`,
        `Which are the types of selectors you can use?`,
        `What is the selector priority hierarchy?`,
        `What is the difference between block elements and inline elements?`,
        `What is the grid layout?`,  
    ],
    'DOM/DOM API': [
        `What is the DOM tree?`,
        `What are the document and window objects?`,
        `What are events?`,
        `What is the difference between event bubbling and event capturing?`,
        `What is DOM API? Why would you use it?`,
    ],
    'HTTP/AJAX': [
        `HTTP / AJAX`,
        `What is a network protocol?`,
        `What is HTTP? How does it work?`,
        `What are HTTP methods, headers and status codes?`,
        `What is the request body?`,
        `What is AJAX? Why to use AJAX?`,
        `What is the fetch API?`,    
    ],
    'Asynchronous Programming': [
        `What is async programming? When would we want to use it?`,
        `What is ‘promise’ and how it helps us?`,
        `What are the async / await keywords?`,
        `What is the event loop?`,           
    ],

};

export const group = ['Debby', 'Kiro', 'Sava', 'Svetlio', 'Vladi', 'Yo'];
